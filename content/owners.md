---
title: "Owner's Resources"
draft: false
description: "Information and documentation for Ventura Villas property owners."
type: "single"
---

### Documents

- [ARC Application](/docs/ARC_Application.pdf)
- [Board Member Application](/docs/Board_Member_Application.pdf)
- [Boat Storage Rules and Lease](/docs/Boat_Storage.pdf)
- [By-Laws](/docs/Bylaws.pdf)
- [Rules and Regulations](/docs/Rules_and_Regulations_2022.pdf)
- [Trash Pickup Schedule](/docs/2024_Waste_Management_Calendar.pdf)
- [Recycling Pickup Schedule](/docs/2024_Recycling_Calendar.pdf)
- [Color Palette](/colors)

### Management

Ventura Villas is managed by Island Realty. Report all problems and requests for service to Brenda Manigault, the Island Realty representative, at [843-886-4041](tel:8438864041) or <brenda@islandrealty.com>.

Timely reporting of any maintenance problem, which threatens the structure, is required, even when the unit is vacant. Roof leaks, termite activity or damage, water leaks and wind damage should be reported as an urgent problem so damage can be limited and repairs begun promptly. In the event that Island Realty cannot be notified or does not respond in a timely manner, the Board President should be notified.

All VVPOA fees and correspondence should be sent to:

```
Brenda Manigault, Island Realty
1304 Palm Boulevard
Isle of Palms, SC 29451
```

Payments may be made by check, money order, cashier’s check or automatic bank draft.

### Board of Administration

The Board of Administration of the Ventura Villas Property Owners Association as defined by the Bylaws is comprised of fifteen (15) persons, all of whom must be owners of units in the property. Board members are elected at the Annual Meeting and serve three year terms. Any owner wishing to serve on the Board should contact the Property Manager.

It is the responsibility of the Board to conduct the affairs of the Association, to make rules and regulations, collect and administer the regime fees and maintain all facilities and property that are of Ventura Villas.

Board meetings are held monthly at the Clubhouse. Residents are welcome to attend and have the opportunity to address the board at the beginning of each meeting. The meeting schedule for 2024 is:

- January 25, 2024
- February 22, 2024
- March 28, 2024 : **Annual Meeting**
- April 25, 2024
- May 23, 2024
- June 27, 2024
- July 25, 2024
- August 22, 2024
- September 26, 2024
- October 24, 2024
- November 28, 2024
- December 26, 2024

#### Board of Administration 2023-24 Term

Ann Whalen, President

Scott Ambler, Vice President

Barbara Mullally, Secretary

Marilyn Birkhimer, Treasurer

Michael Bull, Rodney Davis, Van Hauser, Ann Montgomery, Karen Rice, Leo Russo, Jay Voss, Phil Williams

{{< todo >}}
### Renting the Clubhouse

W.I.P.
{{< / todo >}}
