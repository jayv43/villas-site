---
title: "Memorial Day Party"
date: 2021-05-30 16:00:00
end_date: 2021-05-30 19:00:00
location: At the Ventura Villas Pool and Clubhouse
draft: false
---

You're invited! Please join us to celebrate Memorial Day Weekend at the pool.

_~Featuring Ventura Villas' own, DJ Bobby Carrier, playing your favorite sounds~_

Beach music, rock, country, and more!

Bring your favorite appetizer & drink.

### We hope to see you there!
