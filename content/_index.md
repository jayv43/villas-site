---
draft: false
description: "Ventura Villas at Snee Farm is a condominium community located in central Mt. Pleasant, South Carolina."
type: "single"
---

# Welcome to Ventura Villas at Snee Farm

![View of Golf Course](/pics/golfcourse.png)

Once the home of Charles Pinckney, signer of the US Constitution, Snee Farm is now the site of 160 condominium units nestled between tidal ponds and the beautiful Snee Farm Country Club grounds.

Each building contains eight units with a variety of floor plans. There are single floor and two story-units, ranging from two to four bedrooms. Most have enclosed sunrooms as well as yard areas considered to be “limited common” areas. Mature landscaping adds to the appeal of the grounds. The Villas offer a beautiful swimming pool with club house and a tennis court for the use of the residents. Located along Farm Quarter Road, the Villas have a quiet rural feeling in the heart of Mt. Pleasant. The location is convenient to shopping, the ocean, historic sites and downtown Charleston.

Ventura Villas are governed by Master Deeds, By-laws and Rules and Regulations that can be found on this web site. A board of 15 member residents meets monthly to conduct the business of the Property Owner’s Association (VVPOA). The Annual Meeting of the Property Owner’s Association is held each March. All property owners are voting members of the Association and subject to VVPOA fees and all regulations of the Association. Tenants are also subject to the rules of the Association.

{{< load-leaflet >}}

{{< leaflet-simple mapHeight="420px" maxWidth="100%" mapLon="-79.82660" mapLat="32.83720" markerLon="-79.82660" markerLat="32.83720"zoom="16">}}
